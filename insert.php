<?php 
  //insert.php
  
  include("connect_db.php");

  $form_data = json_decode(file_get_contents("php://input"));

  $message = '';

  $data =  array(
    ':first_name' =>  $form_data->first_name,
    ':last_name'  =>  $form_data->last_name
  );

  $query = "INSERT INTO tbl_sample (first_name, last_name) VALUES (:first_name, :last_name)";
  $stmt = $conn->prepare($query);
  $stmt->bindParam(':first_name', $first_name);
  $stmt->bindParam(':last_name', $last_name);
  if($stmt->execute($data)){
    $message = 'Data Inserted';
  }

  $output = array(
    'message' => $message,
  );

  echo json_encode($output);

 ?>