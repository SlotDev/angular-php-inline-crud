<!DOCTYPE html>
<html>
<head>
  <title>Angular with PHP inline-table (CRUD)</title>

  <script
  src="https://code.jquery.com/jquery-2.2.4.js"
  integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
  crossorigin="anonymous"></script>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.3/angular.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-datatables/0.6.4/angular-datatables.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">

</head>
<body>

  <div class="container">
    <br>
      <h3 align="center">AngularJS PHP inline-table (CRUD)</h3>
    <br>

    <div class="table-responsive" ng-app="liveApp" ng-controller="liveController" ng-init="fetchData()">
      <div class="alert alert-success alert-dismissible" ng-show="success" >
          <a href="#" class="close" data-dismiss="alert" ng-click="closeMsg()" aria-label="close">&times;</a>
          {{successMessage}}
      </div> 
      <form name="testform" ng-submit="insertData()">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Action</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input type="text" ng-model="addData.first_name" class="form-control" placeholder="Enter First Name" ng-required="true" />
                </td>
                <td>
                  <input type="text" ng-model="addData.last_name" class="form-control" placeholder="Enter Last Name" ng-required="true" />
                </td>
                <td>
                  <button type="submit" class="btn btn-success btn-sm" ng-disabled="testform.$invalid">Add</button>
                </td>
              </tr>
              <tr ng-repeat="data in namesData" ng-include="getTemplate(data)"></tr>
                
            </tbody>
        </table>
      </form> 

      <script type="text/ng-template" id="display">
          <td>{{data.first_name}}</td>
          <td>{{data.last_name}}</td>
          <td>
              <button type="button" class="btn btn-primary btn-sm" ng-click="showEdit(data)">Edit</button>
              <button type="button" class="btn btn-danger btn-sm" ng-click="deleteData(data.id)">Delete</button>
          </td>
      </script>
      <script type="text/ng-template" id="edit">
          <td>
            <input type="text" ng-model="formData.first_name" class="form-control"  />
          </td>
          <td>
            <input type="text" ng-model="formData.last_name" class="form-control" />
          </td>
          <td>
              <input type="hidden" ng-model="formData.data.id" />
              <button type="button" class="btn btn-info btn-sm" ng-click="editData()">Save</button>
              <button type="button" class="btn btn-default btn-sm" ng-click="reset()">Cancel</button>
          </td>
      </script>  

    </div>
  </div> <!-- END CONTAINER -->

<script>
  
  var app = angular.module('liveApp', []);

  app.controller('liveController', function($scope, $http){

    $scope.formData = {};
    $scope.addData = {};
    $scope.success = false;

    $scope.getTemplate = function(data){
      if(data.id === $scope.formData.id){
        return 'edit';
      }else{
        return 'display';
      }
    };

    $scope.fetchData = function(){
      $http.get('select.php').success(function(data){
        $scope.namesData = data;
      });
    };

    $scope.insertData = function(){
      $http({
        method:"POST",
        url:"insert.php",
        data:$scope.addData,
      }).success(function(data){
        $scope.success = true;
        $scope.successMessage = data.message;
        $scope.fetchData();
        $scope.addData = {};
      });
    };

    $scope.showEdit = function(data){
      $scope.formData = angular.copy(data);
    };

    $scope.editData = function(){
      $http({
        method:"POST",
        url:"edit.php",
        data:$scope.formData,
      }).success(function(data){
        $scope.success = true;
        $scope.successMessage = data.message;
        $scope.fetchData();
        $scope.formData = {};
      });
    };


    $scope.reset = function(){
      $scope.formData = {};
    };

    $scope.closeMsg = function(){
      $scope.success = false;
    };

    $scope.deleteData = function(id){
      if(confirm("Are you sure you want to remove it?")){
        $http({
          method:"POST",
          url:"delete.php",
          data:{'id':id}
        }).success(function(data){
          $scope.success = true;
          $scope.successMessage =  data.message;
          $scope.fetchData();
        });
      }
    };

  });

  
  ///////////////////// angular 1.6+ //////////////////////////
  // var app = angular.module('crudApp', ['datatables']);
  // app.controller('crudController', function($scope, $http){
  //   $scope.fetchData = function(){
  //   $http({
  //     method: 'GET',
  //     url : 'fetch_data.php'  
  //   }).then(function(data){
  //     console.log(data);
  //     //$scope.namesData = data;
  //   });
  //  };
  // });
  ///////////////////// angular 1.6+ //////////////////////////

</script>

</body>
</html>
